<?php

namespace Drupal\node_revision_limit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure node_revision_limit settings for this site.
 */
class NodeRevisionLimitForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_revision_limit.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'node_revision_limit.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('node_revision_limit.settings');

    $form['limit_number_of_revisions'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
      ),
      '#title' => $this->t('Limit number of node revisions to :'),
      '#default_value' => $config->get('limit_number_of_revisions'),
      '#description' => t('Limit number of node revisions for each language')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration
    $this->config('node_revision_limit.settings')
      // Set the submitted configuration setting
      ->set('limit_number_of_revisions', $form_state->getValue('limit_number_of_revisions'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
