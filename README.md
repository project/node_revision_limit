
# Node Revision Limit

The module allow to choose the number of revisions to keep to nodes.
Once set, revision deletions will occur on each node update to keep that number limit, independently for each language activated.
The module is compatible with the usage of Content Translation and Content Moderation

## How to use

- Activate the module
- Set the number of revisions to keep per node for each activated language ( 3 by default ).
  /admin/config/content/node_revision_limit
- The action of node revision deletion for a node will happen when that node is updated.
